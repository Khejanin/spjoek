// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SpjoekGameMode.h"
#include "SpjoekHUD.h"
#include "SpjoekCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASpjoekGameMode::ASpjoekGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASpjoekHUD::StaticClass();
}
