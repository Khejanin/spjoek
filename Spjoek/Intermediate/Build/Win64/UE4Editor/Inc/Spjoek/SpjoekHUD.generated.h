// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPJOEK_SpjoekHUD_generated_h
#error "SpjoekHUD.generated.h already included, missing '#pragma once' in SpjoekHUD.h"
#endif
#define SPJOEK_SpjoekHUD_generated_h

#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_RPC_WRAPPERS
#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpjoekHUD(); \
	friend struct Z_Construct_UClass_ASpjoekHUD_Statics; \
public: \
	DECLARE_CLASS(ASpjoekHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekHUD)


#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpjoekHUD(); \
	friend struct Z_Construct_UClass_ASpjoekHUD_Statics; \
public: \
	DECLARE_CLASS(ASpjoekHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekHUD)


#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpjoekHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpjoekHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekHUD(ASpjoekHUD&&); \
	NO_API ASpjoekHUD(const ASpjoekHUD&); \
public:


#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekHUD(ASpjoekHUD&&); \
	NO_API ASpjoekHUD(const ASpjoekHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpjoekHUD)


#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Spjoek_Source_Spjoek_SpjoekHUD_h_9_PROLOG
#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_RPC_WRAPPERS \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_INCLASS \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spjoek_Source_Spjoek_SpjoekHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_INCLASS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPJOEK_API UClass* StaticClass<class ASpjoekHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spjoek_Source_Spjoek_SpjoekHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
