// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPJOEK_SpjoekGameMode_generated_h
#error "SpjoekGameMode.generated.h already included, missing '#pragma once' in SpjoekGameMode.h"
#endif
#define SPJOEK_SpjoekGameMode_generated_h

#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_RPC_WRAPPERS
#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpjoekGameMode(); \
	friend struct Z_Construct_UClass_ASpjoekGameMode_Statics; \
public: \
	DECLARE_CLASS(ASpjoekGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Spjoek"), SPJOEK_API) \
	DECLARE_SERIALIZER(ASpjoekGameMode)


#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpjoekGameMode(); \
	friend struct Z_Construct_UClass_ASpjoekGameMode_Statics; \
public: \
	DECLARE_CLASS(ASpjoekGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Spjoek"), SPJOEK_API) \
	DECLARE_SERIALIZER(ASpjoekGameMode)


#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SPJOEK_API ASpjoekGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpjoekGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPJOEK_API, ASpjoekGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPJOEK_API ASpjoekGameMode(ASpjoekGameMode&&); \
	SPJOEK_API ASpjoekGameMode(const ASpjoekGameMode&); \
public:


#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPJOEK_API ASpjoekGameMode(ASpjoekGameMode&&); \
	SPJOEK_API ASpjoekGameMode(const ASpjoekGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPJOEK_API, ASpjoekGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpjoekGameMode)


#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Spjoek_Source_Spjoek_SpjoekGameMode_h_9_PROLOG
#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_RPC_WRAPPERS \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_INCLASS \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spjoek_Source_Spjoek_SpjoekGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPJOEK_API UClass* StaticClass<class ASpjoekGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spjoek_Source_Spjoek_SpjoekGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
