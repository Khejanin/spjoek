// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Spjoek/SpjoekHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpjoekHUD() {}
// Cross Module References
	SPJOEK_API UClass* Z_Construct_UClass_ASpjoekHUD_NoRegister();
	SPJOEK_API UClass* Z_Construct_UClass_ASpjoekHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Spjoek();
// End Cross Module References
	void ASpjoekHUD::StaticRegisterNativesASpjoekHUD()
	{
	}
	UClass* Z_Construct_UClass_ASpjoekHUD_NoRegister()
	{
		return ASpjoekHUD::StaticClass();
	}
	struct Z_Construct_UClass_ASpjoekHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpjoekHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Spjoek,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpjoekHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "SpjoekHUD.h" },
		{ "ModuleRelativePath", "SpjoekHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpjoekHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpjoekHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpjoekHUD_Statics::ClassParams = {
		&ASpjoekHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASpjoekHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASpjoekHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpjoekHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpjoekHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpjoekHUD, 958350512);
	template<> SPJOEK_API UClass* StaticClass<ASpjoekHUD>()
	{
		return ASpjoekHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpjoekHUD(Z_Construct_UClass_ASpjoekHUD, &ASpjoekHUD::StaticClass, TEXT("/Script/Spjoek"), TEXT("ASpjoekHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpjoekHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
