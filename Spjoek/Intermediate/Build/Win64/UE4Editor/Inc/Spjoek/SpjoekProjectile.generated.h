// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef SPJOEK_SpjoekProjectile_generated_h
#error "SpjoekProjectile.generated.h already included, missing '#pragma once' in SpjoekProjectile.h"
#endif
#define SPJOEK_SpjoekProjectile_generated_h

#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpjoekProjectile(); \
	friend struct Z_Construct_UClass_ASpjoekProjectile_Statics; \
public: \
	DECLARE_CLASS(ASpjoekProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpjoekProjectile(); \
	friend struct Z_Construct_UClass_ASpjoekProjectile_Statics; \
public: \
	DECLARE_CLASS(ASpjoekProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpjoekProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpjoekProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekProjectile(ASpjoekProjectile&&); \
	NO_API ASpjoekProjectile(const ASpjoekProjectile&); \
public:


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekProjectile(ASpjoekProjectile&&); \
	NO_API ASpjoekProjectile(const ASpjoekProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpjoekProjectile)


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ASpjoekProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ASpjoekProjectile, ProjectileMovement); }


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_9_PROLOG
#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_RPC_WRAPPERS \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_INCLASS \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spjoek_Source_Spjoek_SpjoekProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPJOEK_API UClass* StaticClass<class ASpjoekProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spjoek_Source_Spjoek_SpjoekProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
