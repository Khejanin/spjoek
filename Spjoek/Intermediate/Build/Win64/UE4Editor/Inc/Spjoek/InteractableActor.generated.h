// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPJOEK_InteractableActor_generated_h
#error "InteractableActor.generated.h already included, missing '#pragma once' in InteractableActor.h"
#endif
#define SPJOEK_InteractableActor_generated_h

#define Spjoek_Source_Spjoek_InteractableActor_h_12_RPC_WRAPPERS \
	virtual void Interact_Implementation(); \
 \
	DECLARE_FUNCTION(execInteract) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Interact_Implementation(); \
		P_NATIVE_END; \
	}


#define Spjoek_Source_Spjoek_InteractableActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void Interact_Implementation(); \
 \
	DECLARE_FUNCTION(execInteract) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Interact_Implementation(); \
		P_NATIVE_END; \
	}


#define Spjoek_Source_Spjoek_InteractableActor_h_12_EVENT_PARMS
#define Spjoek_Source_Spjoek_InteractableActor_h_12_CALLBACK_WRAPPERS
#define Spjoek_Source_Spjoek_InteractableActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAInteractableActor(); \
	friend struct Z_Construct_UClass_AInteractableActor_Statics; \
public: \
	DECLARE_CLASS(AInteractableActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(AInteractableActor)


#define Spjoek_Source_Spjoek_InteractableActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAInteractableActor(); \
	friend struct Z_Construct_UClass_AInteractableActor_Statics; \
public: \
	DECLARE_CLASS(AInteractableActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(AInteractableActor)


#define Spjoek_Source_Spjoek_InteractableActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AInteractableActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AInteractableActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractableActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractableActor(AInteractableActor&&); \
	NO_API AInteractableActor(const AInteractableActor&); \
public:


#define Spjoek_Source_Spjoek_InteractableActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractableActor(AInteractableActor&&); \
	NO_API AInteractableActor(const AInteractableActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractableActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AInteractableActor)


#define Spjoek_Source_Spjoek_InteractableActor_h_12_PRIVATE_PROPERTY_OFFSET
#define Spjoek_Source_Spjoek_InteractableActor_h_9_PROLOG \
	Spjoek_Source_Spjoek_InteractableActor_h_12_EVENT_PARMS


#define Spjoek_Source_Spjoek_InteractableActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_InteractableActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_InteractableActor_h_12_RPC_WRAPPERS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_CALLBACK_WRAPPERS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_INCLASS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spjoek_Source_Spjoek_InteractableActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_InteractableActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_InteractableActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_CALLBACK_WRAPPERS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_INCLASS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_InteractableActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPJOEK_API UClass* StaticClass<class AInteractableActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spjoek_Source_Spjoek_InteractableActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
