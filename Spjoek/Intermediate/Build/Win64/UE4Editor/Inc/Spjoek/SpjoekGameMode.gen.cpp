// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Spjoek/SpjoekGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpjoekGameMode() {}
// Cross Module References
	SPJOEK_API UClass* Z_Construct_UClass_ASpjoekGameMode_NoRegister();
	SPJOEK_API UClass* Z_Construct_UClass_ASpjoekGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Spjoek();
// End Cross Module References
	void ASpjoekGameMode::StaticRegisterNativesASpjoekGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASpjoekGameMode_NoRegister()
	{
		return ASpjoekGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ASpjoekGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpjoekGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Spjoek,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpjoekGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "SpjoekGameMode.h" },
		{ "ModuleRelativePath", "SpjoekGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpjoekGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpjoekGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpjoekGameMode_Statics::ClassParams = {
		&ASpjoekGameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_ASpjoekGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASpjoekGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpjoekGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpjoekGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpjoekGameMode, 2499800993);
	template<> SPJOEK_API UClass* StaticClass<ASpjoekGameMode>()
	{
		return ASpjoekGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpjoekGameMode(Z_Construct_UClass_ASpjoekGameMode, &ASpjoekGameMode::StaticClass, TEXT("/Script/Spjoek"), TEXT("ASpjoekGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpjoekGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
