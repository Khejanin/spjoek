// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPJOEK_SpjoekCharacter_generated_h
#error "SpjoekCharacter.generated.h already included, missing '#pragma once' in SpjoekCharacter.h"
#endif
#define SPJOEK_SpjoekCharacter_generated_h

#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_RPC_WRAPPERS
#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpjoekCharacter(); \
	friend struct Z_Construct_UClass_ASpjoekCharacter_Statics; \
public: \
	DECLARE_CLASS(ASpjoekCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekCharacter)


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASpjoekCharacter(); \
	friend struct Z_Construct_UClass_ASpjoekCharacter_Statics; \
public: \
	DECLARE_CLASS(ASpjoekCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Spjoek"), NO_API) \
	DECLARE_SERIALIZER(ASpjoekCharacter)


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpjoekCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpjoekCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekCharacter(ASpjoekCharacter&&); \
	NO_API ASpjoekCharacter(const ASpjoekCharacter&); \
public:


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpjoekCharacter(ASpjoekCharacter&&); \
	NO_API ASpjoekCharacter(const ASpjoekCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpjoekCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpjoekCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpjoekCharacter)


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ASpjoekCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ASpjoekCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ASpjoekCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ASpjoekCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ASpjoekCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ASpjoekCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ASpjoekCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ASpjoekCharacter, L_MotionController); }


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_11_PROLOG
#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_RPC_WRAPPERS \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_INCLASS \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spjoek_Source_Spjoek_SpjoekCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Spjoek_Source_Spjoek_SpjoekCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPJOEK_API UClass* StaticClass<class ASpjoekCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spjoek_Source_Spjoek_SpjoekCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
